# via-sphere: A Spherical Vector Geometry library

[![License](https://img.shields.io/badge/License-MIT-blue)](https://opensource.org/license/mit/)
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/viaaero/via-sphere/master)

A header-only C++ library with Python bindings for calculating distances,
angles and positions on the surface of a sphere.

## Description

[Spherical vector geometry](docs/Spherical_Vector_Geometry.md) uses a combination
of spherical trigonometry and vector geometry to calculate distances, angles and
positions on the surface of a sphere.

Spherical trigonometry is geometry performed on the surface of a sphere.  
Menelaus of Alexandria established a basis for spherical trigonometry nearly two thousand years ago.
Abu al-Bīrūnī and John Napier (among others) provided equations and tables that have been depended
upon for [global navigation](docs/Global_Navigation.md) across deserts and oceans for
many hundreds of years.

Vector geometry is geometry performed using vectors.  
Vector geometry is relatively new compared to spherical geometry,
being only a few hundred years old.
It is widely used in computer systems and it can also be used to
perform measurements on the surface of a sphere, see:
[n-vector](http://www.navlab.net/nvector/).

## Design

Points on the surface of a sphere are represented by 3D vectors with x, y and z
coordinates see Figure 1.

![Spherical Vector Coordinates](docs/images/ECEF_coordinates.png)  
*Figure 1 Spherical Vector Coordinates*

All distances and angles on the surface of a sphere are measured in radians.  
Physical distances can be calculated by multiplying by the radius of the sphere.

Functions are provided to convert latitude/longitude coordinates into
spherical vectors and back again.  
Latitude and longitude are measured in degrees.  
Note: when representing points on the Earth's surface, the 3D vector coordinates
are in the standard WGS84 [ECEF](https://en.wikipedia.org/wiki/ECEF)
orientation with the z axis between the North and South poles, see Figure 1.

C++ and Python functions are provided to process [std::vector](https://en.cppreference.com/w/cpp/container/vector)s
and [numpy array](https://docs.scipy.org/doc/numpy-1.14.0/reference/generated/numpy.array.html)s
of points and Great Circle arcs.

For more information, see [spherical vector design](docs/Spherical_Vector_Design.md)

## Use

Download or clone the latest tagged version of `via-sphere` from
[BitBucket](https://bitbucket.org/viaaero/via-sphere/src/master/).

### C++

Since it's a header only library, it just requires the location of the
[include](include) directory to be added to the `INCLUDE_PATH`.

It should work with any C++11 compliant compiler.  
It's been tested with GCC 14.2, Apple Clang 8.0.0 and Visual Studio 2022.

### Python

#### Requirements

The Python bindings, use the `pybind11` library which has has specific C++
compiler requirements.  
In particular, `pybind11` requires Visual Studio 2015 update 3 (or newer) on a
Windows machine, or clang 5.0.0 (or newer) on an Apple machine,
see [pybind11](https://github.com/pybind/pybind11).  

#### Installation

From the directory above where `via-sphere` has been downloaded or cloned,
run the following:

    pip install ./via-sphere
	
To verify the installation, run the tests in: [python/tests](python/tests)  
The python bindings have been tested with python 3.12.

#### Importing

Import the software as `via_sphere`, e.g.:

	from via_sphere import Point3d, global_Point3d, distance_radians
	
#### Docker Container

The [Dockerfile](Dockerfile) installs `via-sphere` into a standard
[python](https://hub.docker.com/_/python/) docker base image.  

## License

`via-sphere` is provided under a MIT license, see [LICENSE](LICENSE.txt).

Contact <sphere@via-technology.aero> for more information.
