#ifndef VIA_SPHERE_VECTOR3D_HPP
#define VIA_SPHERE_VECTOR3D_HPP

#pragma once

//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file Vector3d.hpp
/// @brief Contains the via::sphere::Vector3d type.
//////////////////////////////////////////////////////////////////////////////
#include <array>
#include <cmath>

namespace via
{
  /// Provides types and functions for spherical vector geometry.
  namespace sphere
  {
    /// The Vector3d type is an array of 3 doubles.
    using Vector3d = std::array<double, 3u>;

    /// 3d vector dot product function: a . b.
    /// Geometrically this function can be considered as the projection of
    /// one vector onto another. The function is commutative.
    /// The formula for this function is: |a||b| cos theta,
    /// where theta is the angle between the vectors a & b.
    /// For two non-zero vectors, the dot product is:
    /// - positive when theta is acute
    /// - negative when theta is obtuse
    /// - zero when the vectors are perpendicular
    /// @pre the vectors should have a magnitude, i.e. norm > 0.0.
    /// If not, the function will return zero.
    /// @param a, b the two Vector3ds
    /// @return the dot product of the two vectors.
    inline double dot(Vector3d const& a, Vector3d const& b) noexcept
    { return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]; }

    /// 3d vector cross product function: a X b.
    /// Geometrically this function creates a vector that is perpendicular
    /// to both a and b and so normal to the plane containing them.
    /// Note: the operation is NOT commutative: i.e. a X b != b X a.
    /// The magnitude of the vector from this function is: |a||b| sin theta,
    /// where theta is the angle between the vectors a & b.
    /// @pre the vectors should have a magnitude, i.e. norm > 0.0.
    /// If not, the function will return zero.
    /// @param a, b the two Vector3ds
    /// @return the cross product of the two vectors.
    inline Vector3d cross(Vector3d const& a, Vector3d const& b) noexcept
    {
      return Vector3d{a[1] * b[2] - a[2] * b[1],
                      a[2] * b[0] - a[0] * b[2],
                      a[0] * b[1] - a[1] * b[0]};
    }

    /// The squared magnitude of a vector.
    /// @return the squared magnitude of the vector.
    inline double norm(Vector3d const& a) noexcept
    { return dot(a, a); }

    /// The length of the vector.
    /// @return the length of a vector.
    inline double length(Vector3d const& a) noexcept
    { return std::sqrt(norm(a)); }

    /// binary addition operator
    /// Add the vectors.
    /// @param a, b the Vectors to be added
    /// @return the resultant Vector
    inline Vector3d operator+(Vector3d const& a, Vector3d const& b) noexcept
    { return Vector3d{ a[0] + b[0], a[1] + b[1], a[2] + b[2] }; }

    /// binary subtraction operator
    /// Subtract b from a.
    /// @param a, b the Vectors to be subtracted.
    /// @return the resultant Vector
    inline Vector3d operator-(Vector3d const& a, Vector3d const& b) noexcept
    { return Vector3d{ a[0] - b[0], a[1] - b[1], a[2] - b[2] }; }

    /// binary multiplication operator
    /// Scales the coordinates by the scale factor.
    /// @param scale the scale factor
    /// @param a the Vector to be scaled
    /// @return the scaled Vector
    inline Vector3d operator*(double scale, Vector3d const& a) noexcept
    { return Vector3d{scale * a[0], scale * a[1], scale * a[2]}; }

    /// binary multiplication operator
    /// Scales the coordinates by the scale factor.
    /// @param a the Vector to be scaled
    /// @param scale the scale factor
    /// @return the scaled Vector
    inline Vector3d operator*(Vector3d const& a, double scale) noexcept
    { return scale * a; }

    /// binary division operator
    /// Scales the coordinates by the scale factor.
    /// @param a the Vector to be scaled
    /// @param scale the scale factor
    /// @return the scaled Vector
    inline Vector3d operator/(Vector3d const& a, double scale)
    { return Vector3d{a[0] / scale, a[1] / scale, a[2] / scale}; }

    /// normalize function
    /// Scales the coordinates so that x^2 + y^2 = z^2 = 1.0
    /// @param a the Vector to be normalized
    /// @return the normalized Vector
    inline Vector3d normalize(Vector3d const& a)
    { return a / length(a); }

    /// unary minus operator
    inline Vector3d operator-(Vector3d const& a) noexcept
    { return -1.0 * a; }
  }
}

#endif // VIA_SPHERE_VECTOR3D_HPP
