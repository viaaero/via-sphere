//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file test_Vector3dd.cpp
/// @brief Contains unit tests for the via::sphere::Vector3d class.
//////////////////////////////////////////////////////////////////////////////
#include "via/sphere/Vector3d.hpp"
#include <boost/test/unit_test.hpp>
#include <limits>
#include <iostream>

using namespace via::sphere;

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_SUITE(TestVector3d)

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Vector3d_dot_and_cross_functions)
{
  // Test Normal constructed values
  Vector3d point_1{1.0, 2.0, 3.0};
  BOOST_CHECK_EQUAL(1.0, point_1[0]);
  BOOST_CHECK_EQUAL(2.0, point_1[1]);
  BOOST_CHECK_EQUAL(3.0, point_1[2]);

  BOOST_CHECK_EQUAL(14.0, norm(point_1));
  BOOST_CHECK_EQUAL(std::sqrt(14.0), length(point_1));

  Vector3d point_2{3.0, 4.0, 5.0};
  BOOST_CHECK_EQUAL(3.0, point_2[0]);
  BOOST_CHECK_EQUAL(4.0, point_2[1]);
  BOOST_CHECK_EQUAL(5.0, point_2[2]);

  // Test dot product
  BOOST_CHECK_EQUAL(26.0, dot(point_1, point_2));

  // Test cross product
  Vector3d point_6(cross(point_1, point_2));
  BOOST_CHECK_EQUAL(-2.0, point_6[0]);
  BOOST_CHECK_EQUAL(4.0,  point_6[1]);
  BOOST_CHECK_EQUAL(-2.0, point_6[2]);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Vector3d_add_and_subtract_functions)
{
  // Test Normal constructed values
  Vector3d point_1{1.0, 2.0, 3.0};
  Vector3d point_2{3.0, 4.0, 5.0};

  Vector3d point_3(point_1 + point_2);
  BOOST_CHECK_EQUAL(4.0, point_3[0]);
  BOOST_CHECK_EQUAL(6.0, point_3[1]);
  BOOST_CHECK_EQUAL(8.0, point_3[2]);

  Vector3d point_4(point_1 - point_2);
  BOOST_CHECK_EQUAL(-2.0, point_4[0]);
  BOOST_CHECK_EQUAL(-2.0, point_4[1]);
  BOOST_CHECK_EQUAL(-2.0, point_4[2]);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Vector3d_multiply_and_divide_functions)
{
  // Test Normal constructed values
  Vector3d point_1{1.0, 2.0, 3.0};

  Vector3d point_3(2.0 * point_1);
  BOOST_CHECK_EQUAL(2.0, point_3[0]);
  BOOST_CHECK_EQUAL(4.0, point_3[1]);
  BOOST_CHECK_EQUAL(6.0, point_3[2]);

  Vector3d point_4(point_1 * 0.5);
  BOOST_CHECK_EQUAL(0.5, point_4[0]);
  BOOST_CHECK_EQUAL(1.0, point_4[1]);
  BOOST_CHECK_EQUAL(1.5, point_4[2]);

  Vector3d point_5(point_1 / 2.0);
  BOOST_CHECK_EQUAL(0.5, point_5[0]);
  BOOST_CHECK_EQUAL(1.0, point_5[1]);
  BOOST_CHECK_EQUAL(1.5, point_5[2]);

  // Standard array function
  BOOST_CHECK(point_4 == point_5);

  Vector3d point_6(-point_1);
  BOOST_CHECK_EQUAL(-1.0, point_6[0]);
  BOOST_CHECK_EQUAL(-2.0, point_6[1]);
  BOOST_CHECK_EQUAL(-3.0, point_6[2]);

  Vector3d point_7(normalize(point_1));
  BOOST_CHECK_EQUAL(1.0, norm(point_7));
}
//////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE_END()
//////////////////////////////////////////////////////////////////////////////
