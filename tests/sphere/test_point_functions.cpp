//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file test_point_functions.cpp
/// @brief Contains unit tests for the via::sphere::Point3d class.
//////////////////////////////////////////////////////////////////////////////
#include "via/sphere/point_functions.hpp"
#include <boost/test/unit_test.hpp>
#include <limits>
#include <iostream>

using namespace via::sphere;

namespace
{
  // The following constants define an Icosahedron around the Earth.
  const auto GOLDEN_ANGLE(std::atan(2.0));
  const auto LATITUDE(90.0 - RAD_TO_DEG * GOLDEN_ANGLE);

  const std::vector<double> LATITUDES {90.0, LATITUDE, LATITUDE,
                                       LATITUDE, LATITUDE, LATITUDE,
                                       -LATITUDE, -LATITUDE, -LATITUDE,
                                       -LATITUDE, -LATITUDE, -90.0};

  const std::vector<double> LONGITUDES {0.0, 180.0, -1.5 * 72.0,
                                        -0.5 * 72.0, 0.5 * 72.0, 1.5 * 72.0,
                                        2.0 * 72.0, -2.0 * 72.0, -1.0 * 72.0,
                                        0.0, 1.0 * 72.0, 0.0};

  const auto CALCULATION_TOLERANCE(0.0000003); // 0.000 000 3% tolerance
}

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_SUITE(Test_point_functions)

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_calculate_global_Point3ds)
{
  // Test conversion of Icosahedron Lat Long verticies.
  auto SIZE(LATITUDES.size());
  auto points3d(calculate_global_Point3ds(LATITUDES, LONGITUDES));
  BOOST_CHECK_EQUAL(SIZE, points3d.size());

  // Test conversion of Icosahedron Point3d verticies
  auto lats(calculate_latitudes(points3d));
  BOOST_CHECK_EQUAL(SIZE, lats.size());
  auto lons(calculate_longitudes(points3d));
  BOOST_CHECK_EQUAL(SIZE, lons.size());

  // Check that round tripped latitudes and longitudes are close
  for (auto i(0u); i < SIZE; ++i)
  {
    BOOST_CHECK_CLOSE(lats[i], LATITUDES[i], CALCULATION_TOLERANCE);
    BOOST_CHECK_CLOSE(lons[i], LONGITUDES[i], CALCULATION_TOLERANCE);
  }
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_calculate_leg_lengths)
{
  auto points3d(calculate_global_Point3ds(LATITUDES, LONGITUDES));

  auto leg_lengths(calculate_leg_lengths(points3d));
  BOOST_CHECK_EQUAL(LATITUDES.size(), leg_lengths.size());
  BOOST_CHECK_EQUAL(0.0, leg_lengths[0]);

  // Check that leg lengths are close to the GOLDEN_ANGLE
  for (auto i(1u); i < LATITUDES.size(); ++i)
    BOOST_CHECK_CLOSE(GOLDEN_ANGLE, leg_lengths[i], CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_calculate_distances)
{
  auto points3d(calculate_global_Point3ds(LATITUDES, LONGITUDES));

  auto distances(calculate_distances(points3d, points3d[0]));
  BOOST_CHECK_EQUAL(LATITUDES.size(), distances.size());

  // North Pole
  BOOST_CHECK_EQUAL(0.0, distances[0]);

  // The 5 verticies North of the Equator
  for (auto i(1u); i < 6u; ++i)
    BOOST_CHECK_CLOSE(GOLDEN_ANGLE, distances[i], CALCULATION_TOLERANCE);

  // The 5 verticies South of the Equator
  for (auto i(6u); i < LATITUDES.size() -1; ++i)
    BOOST_CHECK_CLOSE(M_PI - GOLDEN_ANGLE, distances[i], CALCULATION_TOLERANCE);

  // South Pole
  BOOST_CHECK_EQUAL(M_PI, distances[11]);
}
//////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE_END()
//////////////////////////////////////////////////////////////////////////////
